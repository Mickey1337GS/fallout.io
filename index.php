<?php require_once("app/core.php"); ?>

<!DOCTYPE html>

<html>

    <head>
        <?= $App->getHead(); ?>
        <title><?= $App->getTitle(); ?></title>
    </head>

    <body>
        <?= $App->getChunk("loading"); ?>
        <?= $App->getChunk("notifications"); ?>
        <?= $App->getChunk("music"); ?>
        <div id="app">
            <div id="game"></div>
            <?= $App->getChunk("gui"); ?>
            <?= $App->getChunk("character"); ?>
            <?= $App->getChunk("options"); ?>
        </div>        
        <?= $App->getChunk("main_menu"); ?>
    </body>

</html>
