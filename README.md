# Cogs & Copper: Falloutъ

Browser 2D Fallout-like game/engine.

# Installation

1. Copy files or clone this repository into your html folder
2. Run ```npm install ``` in said folder
3. Run ```node server.js``` or ```nodejs server.js``` in said folder

# Tech stack

If you experience problems during installation process you may want to check if you have these programs/modules installed.

1. Node.JS
2. Socket.IO
3. Phaser CE
4. MySQL

# Files/Folders

!sources - Folder containing all images, sounds and text files (including sources of said images, sounds and etc.). This is not to be shipped alongside the deploy.
admin - Admin section of the game. Features install procedures and basic player management mechanics.
app - Core app folder. This is for web-side of the app.
chunks - Contains pieces (or chunks) of HTML and CSS code that can be included separately. Quality of life feature mostly.
css - Contains CSS files.
fonts - Contains fonts (TrueType mostly).
i - Contains images.
js - Contains JS scripts.
libs - Contains 3rd party libraries and their map files. Currently: Bootstrap (to be removed), jQuery, Phaser CE and socket.io. FontAwesome should also be put here in the future.
maps - Tiled maps are or will be stored here.
sprites - Contains FONTAWESOME sprites, NOT GAME sprites.
svgs - Contains FONTAWESOME graphics.
template - Contains main template files. For example: head section of index file
webfonts - Contains FONTAWESOME webfont files.
.gitignore - List of files to be ignored during GIT commits.
.htaccess - List of rules for web accessibility. Example: don't allow users to read server.js file
config.php - Main configuration file. Contains port settings, app details etc.
index.php - Main entry point of the app: login screen, game itself etc.
package.json - Description of this app package, like credentials, dependencies etc.
README.md - This is what you are reading right now. All the info and instructions are here.
robots.txt - Instructions for search bots (Google, Bing etc). Currently: DO NOT INDEX AT ALL
server.js - This is the core SERVER file to be run with node. It handles player interactions and core online gameplay.

# Known issues

* When you navigate to /admin/ (or any other path or file) from login screen - page reloads before you could get there. This is caused by socket.io ```javascript on("disconnect")``` event, which is expected behaviour. Needs a workaround before removing "location.reload()" part of code.

* Server checks if user is online before loggin in without checking the validity of the password. Maybe change that?

* Login and Register procedures are generally undefended. This needs to be fixed asap.

* Mobile version of the website is not displayed properly