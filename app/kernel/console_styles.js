module.exports = {
    black: "\x1b[30m",
    white: "\x1b[37m",
    green: "\x1b[32m",
    cyan: "\x1b[36m",
    dim: "\x1b[2m",

    bg_red: "\x1b[41m",
    bg_green: "\x1b[42m",
    bg_blue: "\x1b[44m",
    bg_yellow: "\x1b[43m",
    bg_white: "\x1b[47m",
    bg_black: "\x1b[40m",

    underline: "\x1b[4m",
    escape: "%s\x1b[0m",

    pattern_green: function () {
        return this.bg_green + this.black + this.escape;
    },
    pattern_red: function() {
        return this.bg_red + this.white + this.escape;
    },
    pattern_blue: function(){
        return this.bg_blue + this.white + this.escape;
    },
    pattern_yellow: function(){
        return this.bg_yellow + this.black + this.escape;
    },
    pattern_start: function(){
        return this.green + this.underline + this.bg_black + this.escape;
    },
    pattern_info: function(){
        return this.bg_white + this.black + this.escape;
    },
}
