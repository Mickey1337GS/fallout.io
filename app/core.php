<?php
set_include_path( $_SERVER["DOCUMENT_ROOT"] );
require_once( "config.php" );

class app {
    /* FIELDS/PROPERTIES */
    private $ts;            // Timestamp when app was created
    private $title;         // App title
    private $con;           // Connection variable for MySQL queries
    
    /* GENERAL METHODS */
    function __construct( $args ) {
        $this->title = $args["title"];
        session_start();
    }
    function getTitle(){ return $this->title; }
    function getTimestamp(){ return $this->ts; }
    function getChunk($a){ global $App; include("chunks/$a.php"); }
    function getHead(){ require_once("template/head.php"); }
    
    /* DB FUNCTIONS */    
    function connectToDB(){
        $this->con = new mysqli( "127.0.0.1", DB_USER, DB_PASS );
        if (!$this->con) {
            echo "Error: Unable to connect to MySQL." . PHP_EOL;
            echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
            echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }
    }
    function useDB( $DB ){
        $q = "USE $DB";
        $res = $this->con->query($q);
        if( !$res ) {
            echo "Couldn't use database '$DB'. Does it exist?<br>";
            return false;
        } else {
            echo "Connected to DB: '$DB'<br>";
            return true;
        }
    }
    function createDB( $DB ){
        $q = "CREATE DATABASE $DB";
        $res = $this->con->query($q);
        if ( $res ) {
            echo "Database $DB has been created<br>";
            return true;
        } else {
            echo "Couldn't create database $DB. Do you have the rights?<br>";
            return false;
        }
    }
    function ifTableExists( $name ){
        $q = "SHOW TABLES LIKE '$name'";
        $res = $this->con->query($q);
        if ( $res->num_rows === 0 ) {
            echo "Table '$name' does NOT exist.<br>";
            return false;
        } else {            
            echo "Table '$name' exists.<br>";
            return true;
        }
    }
    function makeTable( $name ){
        $q = 
            "CREATE TABLE IF NOT EXISTS $name (
            id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
            name VARCHAR(32) NOT NULL,
            email VARCHAR(64) NOT NULL,
            password VARCHAR(32) NOT NULL,
            status VARCHAR(32) NOT NULL DEFAULT 'active',
            registered TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
            )  ENGINE=".DB_ENGINE;
        $res = $this->con->query($q);
        if ( $res ) {
            echo "Table '$name' created <br>";
            return true;
        } else {
            echo "Couldn't create table '$name'. Do you have the rights?<br>";
            echo $this->con->error;
            return false;
        }
    }
    function getTable( $name ){
        $q = "SELECT * FROM $name";
        $res = $this->con->query($q);
        if( $res->num_rows > 0 ){
            
            $fields = $res->fetch_assoc();
            echo "<div style='display: flex;'>";
            foreach( $fields as $key => $value ) {                
                echo $this->span($key);                
            }
            echo "</div>";
            
            foreach( $res as $key => $value ) {
                echo "<div style='display: flex;'>";
                echo $this->span( $value["id"] );
                echo $this->span( $value["name"] );
                echo $this->span( $value["email"] );
                echo $this->span( $value["password"] );
                echo $this->span( $value["status"] );
                echo $this->span( $value["registered"] );
                echo "</div>";
            }
        } else {
            
        }
    }
    function addAdmin(){
        $q = "INSERT INTO players ( name, password, email ) VALUES ( 'admin', 'password', 'email@web.com' )";
        $res = $this->con->query($q);
        if( $res ) {
            echo "Admin account created.";
            return true;
        } else {
            echo "Couldn't create admin account.";
            return false;
        }
    }
    function addPlayer( $n, $p, $e ){
        if( empty($n) || empty($p) || empty($e) ) {
            echo "One of the fields are empty. Name, email AND password must contain something";
            return false;
        }
        $isDouble = "SELECT * FROM players WHERE name = '$n'";
        $res = $this->con->query($isDouble);
        if ( $res->num_rows > 0 ) { echo "Player duplicate dound! ($n) Can't create new player<br>"; return false; }
        $q = "INSERT INTO players ( name, password, email ) VALUES ( '$n', '$p', '$e')";
        $res = $this->con->query($q);
    }
    
    /* HTML SHORTCUTS*/    
    function div($a)    { return "<div>$a</div>"; }
    function p($a)      { return "<p>$a</p>"; }
    function h1($a)     { return "<h1>$a</h1>"; }
    function h2($a)     { return "<h2>$a</h2>"; }
    function span($a)   { return "<span style='flex: 1;'>$a</span>"; }
}

$App = new app( [ "title" => GAME_TITLE ] );
