// ON MESSAGE RECEIVED
const inMessage = function (t) {
    if (typeof t == "string") {
        $("#chatbox").append(`<div class='chat_message'>${t.replace(/<[^>]+>/g, '')}</div>`);
    } else {
        $("#chatbox").append(`<div class='chat_message'>${t.data.replace(/<[^>]+>/g, '')}</div>`);
    }
    // Scroll the chat window to bottom
    $('#chatbox').scrollTop($('#chatbox')[0].scrollHeight);
};

// WHEN SERVER SENDS A CONFIRM BOX
const confirm700 = function (d) {
    $(".confirm_message").text(d.data);
    $("#confirm").show();
    App.notify({
        text: "Confirm",
        type: 1,
        style: style_N
    });
};

// WHEN SERVER SENDS AN ALERT BOX
const alert701 = function (d) {
    $(".alert_message").text(d.data);
    $("#alert").show();
    App.notify({
        text: "Alert!",
        type: 1,
        style: style_N
    });
};

const entered = function (d) {
    clearInterval(dS);
    game.state.start("localmap");
};
const disconnect = function (d) {
    App.notify({
        text: "Connection lost",
        type: 5
    });
    location.reload(true);
};

const addPlayer = function (x, y, g, local, wx, wy) {
    let player = game.add.sprite(x, y);

    player.dots = game.add.group();
    player.cross = game.add.sprite(0, 0, "cross");
    player.cross.anchor.set(0.5);
    player.addChild(player.cross);

    let wax, way;

    if (wx != 0) {
        wax = x
    };
    if (wy != 0) {
        way = y
    };

    player.waypoint = game.add.sprite(wax, way, "waypoint");
    player.waypoint.anchor.set(0.5);
    player.waypoint.alpha = 0;

    player.triangleH = game.add.sprite(0, 0, "triangle_highlight");
    player.triangleH.anchor.set(0.5, 1);
    player.triangleH.alpha = 0;
    player.addChild(player.triangleH);

    player.triangle = game.add.sprite(0, 0, "triangle");
    player.triangle.anchor.set(0.5, 1);
    player.triangle.alpha = 0;
    player.triangle.inputEnabled = true;
    player.addChild(player.triangle);

    player.triangleT = game.add.text(0, 0, "", style);
    player.triangleT.anchor.set(0.5, 1.5);
    player.addChild(player.triangleT);

    return player;
};

const start = function (d) {
    d.a.forEach(function (e) {
        let local = (d.i == d.a.indexOf(e));
        let player = addPlayer(e.x, e.y, game, local, e.wX, e.wY);
        game.physics.arcade.enable(player);
        pl.push(player);
    });

    game.camera.bounds.x = -22;
    game.camera.bounds.y = -21;
    game.camera.bounds.width = 1590;
    game.camera.bounds.height = 1538;
    game.p1 = pl[d.i];
    d.m.reverse().forEach(function (e) {
        inMessage(e)
    })
};

const setWaypoint = function (d) {
    let p1p = pl[d.i];
    p1p.waypoint.position.x = d.x;
    p1p.waypoint.position.y = d.y;
    game.world.bringToTop(p1p.waypoint);
};

// LOGIN
const cmd_login = function () {
    // Send a notice
    App.notify({
        text: "Trying to log in",
        type: 1,
        style: style_N
    });

    // Disable login and password inputs
    $("#win_login input").prop("disabled", true);

    // Get login and password input fields; Then get their values;
    let IL = $("#input_login");
    let l = IL.val();
    let IP = $("#input_paswd");
    let p = IP.val();

    // Check if login input field is empty
    if (l == "") {
        // Send an error message
        App.notify({
            text: "Login is empty. Stopping",
            type: 5,
            style: style_E
        });
        // Enable login and password inputs to try again.
        $("#win_login input").prop("disabled", false);
        // Exit function so nothing gets submitted
        return "failed";
    }

    // Check if password input field is empty
    if (p == "") {
        // Send an error message
        App.notify({
            text: "Password is empty. Stopping",
            type: 5,
            style: style_E
        });
        // Enable login and password inputs to try again.
        $("#win_login input").prop("disabled", false);
        // Exit function so nothing gets submitted
        return "failed";
    }

    // If both login and password AREN'T empty
    if (l !== "" && p !== "") {
        // Clear password input
        $("#input_paswd").val("");
        // Send login and password to server
        socket.emit("cmd_login", {
            l: l,
            p: p
        });
    }
}

// REGISTER
const cmd_register = function () {
    $("#win_register input").prop("disabled", true);
    let l = $("#reg_login").val();
    let p = $("#reg_password").val();
    let e = $("#reg_email").val();

    App.notify({
        text: "Registering: " + l + ". E-mail: " + e,
        type: 1,
        style: style_N
    });

    socket.emit("cmd_register", {
        l: l,
        p: p,
        e: e
    });
}

// On successful LOG IN
const login_OK = function () {
    // Send a notice
    App.notify({
        text: "Logged in!",
        type: 2,
        style: style_O
    });
    // Hide login screen
    $("#screen_login").fadeToggle();
    //Start game on stage "worldmap"
    game.state.start('worldmap');
}

// IF LOGIN FAILED
const login_NO = function (d = {
    errorcode: -1
}) {
    let message;
    switch (d.errorcode) {
        case 0:
            message = "No such user";
            break;
        case 1:
            message = "User already online";
            break;
        case 2:
            message = "Incorrect password";
            break;
        case 4:
            message = "This user is banned";
            break;
        default:
            message = "Login and/or password are incorrect";
            break;
    }
    // Send an error message
    App.notify({
        text: message,
        type: 5,
        style: style_E
    });
    // Re-enable login and password inputs
    $("#win_login input").prop("disabled", false);
}

// IF REG SUCCESSFUL
const reg_OK = function (d) {
    let message = "Successfully registered"
    // Send a notice
    App.notify({
        text: message,
        type: 2,
        style: style_N
    });
    $("#win_register input").prop("disabled", false);
}

// IF REG FAILED
const reg_NO = function (d = {
    errorcode: -1
}) {
    let message;
    switch (d.errorcode) {
        case 0:
            message = "?";
            break;
        case 1:
            message = "User with this name already exists";
            break;
        case 2:
            message = "Not suitable password";
            break;
        case 3:
            message = "Not suitable username";
            break;
        case 5:
            message = "Not suitable e-mail";
            break;
        default:
            message = "Unhandled error";
            break;
    }
    // Send an error message
    App.notify({
        text: message,
        type: 5,
        style: style_E
    });
    // Re-enable login and password inputs
    $("#win_register input").prop("disabled", false);
}

// WHEN CONNECTED
const connect = function () {
    App.notify({
        text: "Connected!",
        type: 2
    });
    log('%c Welcome to Cogs & Copper: Falloutъ Online! Version: ' + version, style2);
    $("#loading").fadeOut(500);
}

// IF CANNOT CONNECT
const connect_error = function () {
    App.notify({
        text: "Failed to connect to server. Retrying...",
        type: 5
    });
}
