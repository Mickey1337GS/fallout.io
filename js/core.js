"use strict";

const Q = function (a) {
    return document.querySelector(a);
}

class app {
    constructor(args) {
        this.title = args.title;
        this.timeStamp = +new Date();
    }

    notify(args) {
        /* SEND A NOTIFICATION MESSAGE AND LOG IT IN THE CONSOLE */
        console.log("%c" + args.text, args.style);

        var e = document.createElement('div');
        e.className = "note";
        switch (args.type) {
            case 1:
                e.className += " notice";
                break;
            case 2:
                e.className += " ok";
                break;
            case 5:
                e.className += " error";
                break;
            default:
                break;
        }
        e.innerHTML = args.text;
        Q("#notifications").appendChild(e);
        setInterval(() => {
            let element = $(e);
            element.fadeOut(3000, () => {
                element.remove()
            });
        }, 3000)
    }
}

const App = new app({
    title: "Fallout App"
});
