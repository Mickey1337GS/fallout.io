"use strict";

/*  =======================  */
/*  Cogs & Copper: Falloutъ  */
/*  Created: 03.12.2018      */
/*  Updated: 06.08.2019      */
/*  Version: 1.0.0 - Dust    */
/*  =======================  */

const DEBUG = false;

var config = {
    type: Phaser.CANVAS || Phaser.AUTO,     // Fix for older versions, when camera follow introduced lag.
    parent: 'game',
    width: 640,
    height: 480,
    zoom: 1,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 0 }
        }
    },
    scene: [ menu, worldmap, localmap ]
};

const game = new Phaser.Game(config);

const style = {
    font: "16px Fallout Info, sans-serif",
    fill: "#3CF800",
    align: "center"
}

const style2    = 'color: #6E6; background: #222; border-radius: 4px; padding: 4px 16px;';
const style_E   = 'color: #FFF; background: #F00; border-radius: 4px; padding: 4px 16px;';
const style_N   = 'color: #FFF; background: #06F; border-radius: 4px; padding: 4px 16px;';
const style_O   = 'color: #222; background: #0F0; border-radius: 4px; padding: 4px 16px;';
const style_W   = 'color: #222; background: #FE0; border-radius: 4px; padding: 4px 16px;';

$(function() {
    $("#win_options").hide();
    $("#win_character").hide();

    game.state.add('menu', menu);
    game.state.add('worldmap', worldmap);
    game.state.add('localmap', localmap);

    $(".abs").fadeIn(2000, function() { $("#ui_login").fadeIn(2000); });
    $("#options").click(function() { $("#win_options").toggle(); });
    $("#character").click(function() { $("#win_character").toggle(); });
    $("#chat_send").on( "click", sendMessage );
    $("#confirm_yes").click(function() { $("#confirm").hide(); });
    $("#confirm_no").click(function() { $("#confirm").hide(); });
    $("#alert_done").click(function() { $("#alert").hide(); });
    $("#button_inv").click(function() { $("#inventory").toggle(); });
    $("#button_cha").click(function() { $("#character").toggle(); });
    
    $("#btn_login").click(cmd_login);
    $("#btn_register").click(cmd_register);
});

/*

Courtesy of shamansir

Description: Parse and convert any SVG path to the list of commands with JavaScript + Regular Expressions
Link: https://gist.github.com/shamansir/0ba30dc262d54d04cd7f79e03b281505

*/

var sea = "m 0,74.083267 5.6368171,7.091868 6.3133089,7.242199 2.930851,2.431593 -0.35719,4.197062 0.995792,2.99441 -1.635007,2.994411 1.74745,2.61858 7.018454,4.68546 11.002236,2.95665 -10.419993,5.81294 -0.723616,4.30963 7.260245,1.34639 2.599971,11.26826 9.139388,5.63083 5.080439,8.18646 8.15441,6.06212 2.817645,14.78134 13.115348,8.3923 -0.339316,13.27802 2.157594,11.79094 -2.953675,10.43797 9.911339,9.67334 3.321219,0.25142 1.291745,-3.50686 4.974865,0.0259 2.559116,-1.19617 4.738922,6.3204 8.422042,2.63728 6.10809,2.63242 5.73225,0.60295 7.68657,1.73043 3.40212,-1.65202 4.11047,0.8146 2.98299,1.56626 3.58431,1.04012 3.35881,2.76892 3.88498,4.42255 3.43398,6.9782 7.26743,5.09905 3.05815,4.04673 3.96014,0.88977 3.50915,-3.09402 0.72802,-3.09401 4.71179,-3.84567 3.73465,0.063 1.8555,2.61858 -1.15113,4.27223 1.17901,2.24275 11.59149,3.1645 3.30006,3.16449 1.88789,14.22139 7.14948,4.60018 15.58329,-4.65476 5.69735,0.23505 4.14728,-3.62246 3.98483,0.76173 2.70923,4.69484 -3.89246,4.53461 -4.74286,2.62121 3.69186,10.91409 8.14696,3.71221 7.72175,6.90123 10.30644,20.38498 5.37872,6.81003 2.22176,7.1107 2.82308,6.81003 5.52905,4.40473 -0.16694,4.23184 1.71221,3.78085 -0.99376,3.78084 1.18605,4.23184 2.03821,3.84852 2.33888,11.36509 1.88788,11.96642 H 0 Z";

var markerRegEx = /[MmLlSsQqLlHhVvCcSsQqTtAaZz]/g;
var digitRegEx = /-?[0-9]*\.?\d+/g;

function svgPathToCommands(str) {
    var results = [];
    var match;
    while ((match = markerRegEx.exec(str)) !== null) {
        results.push(match);
    };
    return results
        .map(function(match) {
            return {
                marker: str[match.index],
                index: match.index
            };
        })
        .reduceRight(function(all, cur) {
            var chunk = str.substring(cur.index, all.length ? all[all.length - 1].index : str.length);
            return all.concat([{
                marker: cur.marker,
                index: cur.index,
                chunk: (chunk.length > 0) ? chunk.substr(1, chunk.length - 1) : chunk
            }]);
        }, [])
        .reverse()
        .map(function(command) {
            var values = command.chunk.match(digitRegEx);
            return {
                marker: command.marker,
                values: values ? values.map(parseFloat) : []
            };
        })
}

function commandsToSvgPath(commands) {
    return commands.map(function(command) {
        return command.marker + ' ' + command.values.join(',');
    }).join(' ').trim();
}