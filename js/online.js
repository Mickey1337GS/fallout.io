var dS = this.dotSpawn;
pl = [];

$(function () {

    App.notify({
        text: "Connecting to server...",
        type: 1
    });

    socket = io.connect(window.location.host + ":" + port, {
        reconnection: true,
        secure: true
    });

    socket.on("connect", connect);
    socket.on("connect_error", connect_error);
    socket.on("disconnect", disconnect);


    socket.on("newPlayer", function (d) {
        let player = addPlayer(d.data.x, d.data.y);
        game.physics.arcade.enable(player);
        pl.push(player);
        player.name = "asd"
    });

    socket.on("updatePlayer", function (d) {
        let p = pl[d.i];
        p.position.x = d.x;
        p.position.y = d.y;
        p.waypoint.position.x = d.wx;
        p.waypoint.position.y = d.wy;
    });

    socket.on("player_left", function (d) {
        pl[d.i].cross.kill();
        pl[d.i].waypoint.kill();
        pl[d.i].triangle.kill();
        pl[d.i].triangleH.kill();
        pl[d.i].kill();
        pl.splice(d.i, 1);
    });

    const start_game = function () {
        game.input.onDown.add(function () {
            let p = game.p1;
            if (p.triangle) {
                if (!p.triangle.input.pointerOver() || (p.body.velocity.x != 0 && p.body.velocity.y != 0)) {
                    socket.emit('setWaypoint', {
                        x: parseInt(game.input.x + game.camera.position.x),
                        y: parseInt(game.input.y + game.camera.position.y)
                    });
                } else {
                    socket.emit("enter", {
                        x: parseInt(p.position.x),
                        y: parseInt(p.position.y)
                    });
                };
            };
        }, this);
    }

    socket.on("start", start, this);
    socket.on("setWaypoint", setWaypoint, this);
    socket.on("100_chat_message", inMessage, this);
    socket.on("300_kick", function () {
        location.reload();
    }, this)
    socket.on("700_confirm", confirm700, this);
    socket.on("701_alert", alert701, this);
    socket.on("enter", entered, this);
    socket.on("login_OK", login_OK, this);
    socket.on("login_NO", login_NO, this);
    socket.on("reg_OK", reg_OK, this);
    socket.on("reg_NO", reg_NO, this);
    socket.on("start_game", start_game, this);
});
