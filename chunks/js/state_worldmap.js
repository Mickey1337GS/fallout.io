var worldmap = function(game) {};

worldmap.prototype = {

    preload: function() {
        game.stage.disableVisibilityChange = true;
        game.forceSingleUpdate = false;
        game.load.image('waypoint', 'i/waypoint.png');
        game.load.image('location', 'i/location.png');
        game.load.image('triangle', 'i/triangle.png');
        game.load.image('map', 'i/worldmap.png');
        game.load.image('triangle_highlight', 'i/triangle_highlight.png');
        game.load.image('cross', 'i/cross.png');
        game.load.image('dot', 'i/dot.png');
        game.load.image('worldmap_gui', 'i/worldmap_gui.png');
        game.stage.backgroundColor = "#222";
        this.sWidth = 50;
        game.stage.disableVisibilityChange = true;
    },

    create: function() {
        game.time.advancedTiming = true;
        game.time.desiredFps = 35;
        game.forceSingleUpdate = false;
        game.forceSingleRender = false;

        let wSize = 30;
        let hSize = 28;
        let sHeight = this.sWidth;
        let sWidth = this.sWidth;        
        
        /* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
        
        a = svgPathToCommands( sea );       

        let points = [];
        let pX, pY;
        //let m = 7.56;
        let m = 3.78;
        let aStart;
        
        a.forEach(function(v){
            switch(v.marker) {
                case "M":
                    if(DEBUG){ log("%c M - moveTo: " + v.values[0] * m +"/"+ v.values[1] * m, style_W ); };
                    points.push( new Phaser.Point( v.values[0] * m, v.values[1] * m ) );
                    break;
                case "m":
                    if(DEBUG){ log("%c m - moveTo: " + v.values[0] * m +"/"+ v.values[1] * m, style_W ); };
                    points.push( new Phaser.Point( v.values[0] * m, v.values[1] * m - 280 ) );
                    
                    if( v.values.length > 2 ) {
                        for( var i = 0; i < v.values.length; i=i+2 ){
                            pX = points[points.length - 1].x; pY = points[points.length - 1].y;
                            if(DEBUG){
                                log("%c l - lineTo: " + (parseInt(pX) + parseInt(v.values[0+i])) + "/" + (parseInt(pY) + v.values[1+i]), style_W );
                            };
                            points.push( new Phaser.Point( pX + v.values[0+i] * m, pY + v.values[1+i] * m ) );
                        }                        
                    } else {
                        pX = points[points.length - 1].x; pY = points[points.length - 1].y;
                        log("l - lineTo: " + (parseInt(pX) + parseInt(v.values[0] * m)) + "/" + (parseInt(pY) + v.values[1] * m));
                        points.push( new Phaser.Point( pX + v.values[0] * m, pY + v.values[1] * m ) );
                    }
                    break;
                case "V":
                    pX = points[points.length - 1].x;
                    if(DEBUG){ log("v - vertical: " + v.values[0] ); }
                    points.push( new Phaser.Point( pX, v.values[0] * m ) );
                    break;
                case "v":
                    pX = points[points.length - 1].x; pY = points[points.length - 1].y;
                    if(DEBUG){ log("v - vertical: " + v.values[0] * m ); }
                    points.push( new Phaser.Point( pX, pY + v.values[0] * m ) );
                    break;
                case "H":
                    pX = points[points.length - 1].x; pY = points[points.length - 1].y;
                    if(DEBUG){ log("%c H - horizontal (absolute): " + v.values[0] * m, style_W ); }
                    points.push( new Phaser.Point( v.values[0] * m, pY ) );
                    break;
                case "h":
                    pX = points[points.length - 1].x; pY = points[points.length - 1].y;
                    if(DEBUG){ log("%c h - horizontal: " + v.values[0] * m, style_W ); }
                    points.push( new Phaser.Point( pX + v.values[0] * m, pY ) );
                    break;
                case "l":
                    if( v.values.length > 2 ) {
                        for( var i = 0; i < v.values.length; i=i+2 ){
                            pX = points[points.length - 1].x; pY = points[points.length - 1].y;
                            if(DEBUG){
                                log("%c l - lineTo: " + (parseInt(pX) + parseInt(v.values[0+i])) + "/" + (parseInt(pY) + v.values[1+i]), style_W );
                            };
                            points.push( new Phaser.Point( pX + v.values[0+i] * m, pY + v.values[1+i] * m ) );
                        }                        
                    } else {
                        pX = points[points.length - 1].x; pY = points[points.length - 1].y;
                        log("%c l - lineTo: " + (parseInt(pX) + parseInt(v.values[0] * m)) + "/" + (parseInt(pY) + v.values[1] * m), style_W );
                        points.push( new Phaser.Point( pX + v.values[0] * m, pY + v.values[1] * m ) );
                    }
                    break;
                case "Z":
                    aStart = points[0];
                    if(DEBUG){ log("%c Z - back to start: "+aStart.x+"/"+aStart.y, style_W ); }
                    points.push( new Phaser.Point( aStart.x, aStart.y) );
                    break;
                case "z":
                    aStart = points[0];
                    if(DEBUG){ log("%c z - back to start: "+aStart.x+"/"+aStart.y, style_W ); }
                    points.push( new Phaser.Point( aStart.x, aStart.y) );
                    break;
                default:
                    log(v)
                    break;
            }
        });
        
        var poly = new Phaser.Polygon();
        game.poly = poly;
        poly.setTo(points);
        
        polyGraphics = game.add.graphics( 0, 0 );
        polyGraphics.alpha = 0.2;

        polyGraphics.beginFill(0xFF33ff);
        polyGraphics.drawPolygon( poly.points );
        polyGraphics.endFill();

        /* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
        /* LOCATIONS */

        this.world = game.add.sprite(0,0,"map");
        this.locations = game.add.group();
        addLocation(4, 2, "Military Base", game, this.locations);
        addLocation(17, 2, "Vault 13", game, this.locations);
        addLocation(22, 2, "Shady Sands", game, this.locations);
        addLocation(26, 2, "Vault 15", game, this.locations);
        addLocation(23, 4, "Raiders", game, this.locations);
        addLocation(13, 10, "Brotherhood", game, this.locations);
        addLocation(18, 11, "Junktown", game, this.locations);
        addLocation(23, 14, "Necropolis", game, this.locations);
        addLocation(18, 15, "The Hub", game, this.locations);
        addLocation(16, 19, "Boneyard", game, this.locations);
        addLocation(16, 21, "Cathedral", game, this.locations);
        addLocation(25, 26, "The Glow", game, this.locations);
        this.worldmap_gui = game.add.image(0,0,"worldmap_gui");
        this.worldmap_gui.fixedToCamera = true;

        /* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */

        this.fogOfWar = game.add.group();
        this.map = game.add.graphics(0, 0);
        this.map.lineStyle(1, 0x50a1ff, 1);
        this.map.alpha = 0.125;                

        
        for (var i = 0; i < hSize; i++) {
            for (var v = 0; v < wSize; v++) {
                let fog = game.add.graphics( i * sWidth, v * sHeight );
                fog.beginFill( 0x000000 );
                fog.drawRect( 0, 0, sWidth, sHeight );
                fog.endFill();
                this.map.drawRect(i * sWidth, v * sHeight, sWidth, sHeight);
                this.fogOfWar.add(fog);
                if( DEBUG ) { fog.alpha = 0.5 };
            };
        };
        
        
        game.world.setBounds(0, 0, hSize * sWidth, wSize * sWidth );

        /* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */

        function addLocation(x, y, n, g, l) {
            let location = g.add.sprite( ( x - 1 ) * sWidth, ( y - 1 ) * sWidth, 'location' );
            location.alpha = 0.5;
            location.name = n;
            let text = g.add.text( sWidth * 0.5, sWidth, n, { font: "16px Fallout Info", fill: "#3CF800", boundsAlignH: "center" } )
            location.addChild( text );
            text.anchor.set( 0.5, 0 )
            l.add(location);
        };
        
        this.dotSpawn = setInterval(function(){
            if( $("#check_rTrail")[0].checked ){
                pl.forEach(function(p){
                    if( p && p.body ){
                        if( p.body.velocity.x != 0 && p.body.velocity.y != 0 ){
                            let dot = game.add.sprite( p.position.x, p.position.y, "dot" );
                            p.dots.add(dot);
                        } else {
                            p.dots.forEach(function (c) { c.kill(); });
                        };
                    };
                });
            };
        },500);
        
        socket.emit("ready_world");
    },

    update: function() {
        let player = game.p1;
        
        game.world.bringToTop( polyGraphics );
        game.world.bringToTop( this.fogOfWar );
        game.world.bringToTop( this.map );
        
        let speed = 16;
        if( DEBUG ) { speed = 256 };
        
        let prex, prey;
        
        if( player ){
            prex = player.x;
            prey = player.y;
            game.camera.focusOnXY( prex + 84 - 11, prey + 10);
        }

        pl.forEach( function( p ) {
            let c = game.physics.arcade.distanceBetween( p, p.waypoint );
            if (c > speed / 16 ) {
                game.physics.arcade.moveToObject(p, p.waypoint, speed);
                p.cross.alpha = 1;
                p.waypoint.alpha = 1;
                p.triangle.alpha = 0;
                if( game.poly.contains(  p.x + p.body.velocity.x/16, p.y + p.body.velocity.y/16 ) ) {
                    p.x = prex; p.y = prey; p.waypoint.x = prex; p.waypoint.y = prey;
                    p.body.velocity.x = 0; p.body.velocity.y = 0;
                }
            } else {
                p.cross.alpha = 0;
                p.waypoint.alpha = 0;
                p.triangle.alpha = 1;
                try {
                    p.body.velocity.x = 0;
                    p.body.velocity.y = 0;
                } catch {}
            };
        });
        
        if ( player ) {            
            game.world.bringToTop( player.waypoint );
            game.world.bringToTop( player );
            if (player.triangle.input.pointerOver() && player.waypoint.alpha == 0) {
                this.game.canvas.style.cursor = "pointer";
                player.triangleH.alpha = 1;
                player.triangleT.alpha = 1;
            } else {
                this.game.canvas.style.cursor = "default";
                player.triangleH.alpha = 0;
                player.triangleT.alpha = 0;
            };
        };

        let L = this.locations;
        let sWidth = this.sWidth;
        
        this.fogOfWar.forEach(function(c) {
            if (player) {
                if (player.x >= c.x && player.x <= c.x + sWidth && player.y >= c.y && player.y <= c.y + sWidth) { c.kill(); }
                try { player.triangleT.setText( player.name ) }
                catch (e) { console.log("Couldn't resolve socket id") }
                L.forEach(function(o) {
                    let c = game.physics.arcade.distanceToXY(player, o.x, o.y);
                    if (c < sWidth / 2) { player.triangleT.setText(o.name) };
                });

            }
        });        
        
        game.world.bringToTop( this.worldmap_gui );
    },

    render: function() {}

}