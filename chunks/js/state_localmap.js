var localmap = function (game) {};

localmap.prototype = {

    preload: function () {
        game.load.image("hex", "i/hex.png");
        game.load.image("player", "i/player.png");
    },

    create: function () {
        let hex = game.cache.getImage("hex");
        let map_width = 16;
        let map_height = 8;

        let localMap = game.add.group();
        mapArray = [];

        for (var x = 0; x < map_width; x++) {
            mapArray[x] = [];
            for (var y = 0; y < map_height; y++) {
                let offsetX = 0;
                if (y % 2) {
                    offsetX = 32
                };
                // let hx = game.add.sprite( x * hex.width, y * hex.height, 'hex' );
                // let hx = game.add.sprite( x * hex.width + offsetX, y * hex.height, 'hex' );
                let hx = game.add.sprite(x * hex.width + offsetX, y * hex.height - (y * 8), 'hex');

                hx.offsetX = offsetX;
                hx.inputEnabled = true;
                hx.lx = x;
                hx.ly = y;
                hx.events.onInputDown.add(function () {
                    //log( hx.lx + "/" + hx.ly );
                    waypoint.position = hx.position;
                    waypoint.px = hx.lx;
                    waypoint.py = hx.ly;
                });
                let t = game.add.text(hex.width / 2, hex.height / 2 + 3, x + "/" + y, {
                    font: "16px Arial",
                    fill: "#666"
                });
                t.anchor.set(0.5, 0.5);
                hx.addChild(t);
                localMap.add(hx);
                mapArray[x].push({
                    x: hx.x,
                    y: hx.y
                });
            };
        };

        localMap.alpha = 0.25;

        function moveToHex() {
            let dx = Math.abs(waypoint.px - p.px);
            let dy = Math.abs(waypoint.py - p.py);

            if (dx > dy) {
                if (p.px < waypoint.px) {
                    p.px++;
                }
                if (p.px > waypoint.px) {
                    p.px--;
                }
            }

            if (dx <= dy) {
                if (p.py % 2) {
                    if (p.px < waypoint.px) {
                        p.px++;
                    }
                } else {
                    if (p.px > waypoint.px) {
                        p.px--;
                    }
                }
                if (p.py < waypoint.py) {
                    p.py++;
                }
                if (p.py > waypoint.py) {
                    p.py--;
                }
            }

            p.position = mapArray[p.px][p.py];

            // log( `Player: ${p.px}/${p.py} Waypoint: ${waypoint.px}/${waypoint.py}` );
            if (p.px == 0 && p.py == 4 && game.state.current == "localmap" ) {
                game.state.start("worldmap");
            }
        };

        /*
        let px = game.rnd.integerInRange( 0, map_width );
        let py = game.rnd.integerInRange( 0, map_height );
        */

        let pSpawnX = 0;
        let pSpawnY = 0;

        this.player = game.add.sprite(pSpawnX, pSpawnY, "player");
        this.player.u = game.add.sprite(pSpawnX, pSpawnY, "hex");
        this.player.w = game.add.sprite(pSpawnX, pSpawnY, "hex");
        this.player.addChild(this.player.u);
        this.player.anchor.set(-0.25, 0.65);
        let waypoint = this.player.w;
        waypoint.px = pSpawnX;
        waypoint.py = pSpawnY;

        let p = this.player;

        this.moveCD = setInterval(moveToHex, 384);
        this.player.px = pSpawnX;
        this.player.py = pSpawnY;

    },

    update: function () {},

    render: function () {}

}
