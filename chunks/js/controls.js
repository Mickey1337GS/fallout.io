// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //
$(document).keypress(function(e) {
    switch (e.which) {
        case 13: // Enter
            if ($("#chat_input").is(":focus")) { sendMessage() }
            break;
        case 27: // Escape
            break;
        case 112: // F1 - Info
            break;
        case 116: // T - Write text in chat
            break;
        default:
            if ( DEBUG ) { log(e.which) }
            break;
    }
});

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //
function sendMessage() {
    if ($("#chat_input").val().trim() != '') {
        socket.emit("101_send_chat", {
            data: $("#chat_input").val()
        });
    };
    $("#chat_input").val('');
};

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- //