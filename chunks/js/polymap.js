var polyMap = {
   "width": "64",
   "height": "64",
   "viewBox": "0 0 64 64",
   "version": [
      "1.1",
      "0.92.3 (2405546, 2018-03-11)"
   ],
   "id": "svg2004",
   "docname": "test.svg",
   "defs": {
      "id": "defs1998"
   },
   "namedview": {
      "id": "base",
      "pagecolor": "#ffffff",
      "bordercolor": "#666666",
      "borderopacity": "1.0",
      "pageopacity": "0.0",
      "pageshadow": "2",
      "zoom": "5.6",
      "cx": "25.403679",
      "cy": "46.800035",
      "document-units": "px",
      "current-layer": "layer1",
      "showgrid": "false",
      "units": "px",
      "showpageshadow": "false",
      "window-width": "1920",
      "window-height": "1017",
      "window-x": "1912",
      "window-y": "-8",
      "window-maximized": "1",
      "scale-x": "1"
   },
   "metadata": {
      "id": "metadata2001",
      "RDF": {
         "Work": {
            "about": "",
            "format": "image/svg+xml",
            "type": {
               "resource": "http://purl.org/dc/dcmitype/StillImage"
            },
            "title": null
         }
      }
   },
   "g": {
      "label": "Layer 1",
      "groupmode": "layer",
      "id": "layer1",
      "transform": "translate(0,-306.54165)",
      "path": {
         "style": "fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.80125988;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1",
         "d": "m 0,370.54165 v -64 h 32 l 32,64 z",
         "id": "path2549",
         "connector-curvature": "0",
         "nodetypes": "ccccc"
      }
   }
}