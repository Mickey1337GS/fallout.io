// Disable phaser.io console banner.
window.PhaserGlobal = { hideBanner: true };

// General variables
var log = console.log;
var version = "1.0.0 - Dust";
var ip = "192.168.1.65";        // IP address is for testing purposes only.
var port = 1337;