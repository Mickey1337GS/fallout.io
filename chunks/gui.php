<div class="gui" id="confirm">
    <div class="confirm_message"></div>
    <button id="confirm_yes"></button><!--
    --><button id="confirm_no"></button>
</div>

<div class="gui" id="alert">
    <div class="alert_message"></div>
    <button id="alert_done"></button>
</div>

<div id="chatControls">
    <input id="chat_input" type="text" class="fF" maxlength="81" placeholder="Enter text to chat...">
    <button id="chat_send" class="fF"><i class="fa fa-comment"></i></button>
</div>

<div class="gui" id="main">
    <div id="chatbox"></div>
    <div id="inventory"></div>
    <div id="character"></div>
    <button class="gui_button" id="button_swap"></button>
    <button class="gui_button" id="button_skilldex"></button>
    <button class="gui_button" id="button_inv"></button>
    <button class="gui_button" id="button_options"></button>
    <button class="gui_button" id="button_map"></button>
    <button class="gui_button" id="button_cha"></button>
    <button class="gui_button" id="button_pip"></button>
</div>