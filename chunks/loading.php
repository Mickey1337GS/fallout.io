<style>
    #loading {
        position: fixed;
        top: 0; right: 0; bottom: 0; left: 0;
        background: #000;
        z-index: 6;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    #loading_icon {
        width: 240px;
        text-align: center;
    }

    #loading_icon h1 {
        font-family: 'Poiret One';
        color: #2B2;
        text-shadow: 0 0 16px #2B2;
        animation: blinking 1s infinite alternate;
    }

    #loading_icon img {
        width: 100%;
        -webkit-animation: rotation 60s infinite linear;
        animation: rotation 60s infinite linear;
    }
    
    @-webkit-keyframes rotation {
        from { -webkit-transform: rotate(0deg); }
        to { -webkit-transform: rotate(360deg); }
    }
    
    @keyframes rotation {
        from { transform: rotate(0deg); }
        to { transform: rotate(360deg); }
    }
    
    @keyframes blinking {
        from { opacity: 1; }
        to { opacity: 0; }
    }

</style>


<div id="loading">
    <div id="loading_icon">
        <img src="/i/reverseGear.png">
        <h1>LOADING</h1>
    </div>
</div>
