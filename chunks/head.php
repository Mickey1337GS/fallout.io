<meta charset="utf-8">
<title>Cogs & Copper: Falloutъ</title>
<link rel="shortcut icon" href="/i/favicon.ico" type="image/x-icon">
<link rel="icon" href="/i/favicon.ico" type="image/x-icon">
        
<meta name="viewport" content="width=device-width, initial-scale=1">
        
<script src="/libs/jquery-3.3.1.min.js"></script>
<script src="/libs/phaser.min.js"></script>
<script src="/libs/socket.io.slim.js"></script>

<script src="/js/online.js"></script>
<script src="/js/functions.js"></script>
<script src="/chunks/js/state_menu.js"></script>
<script src="/chunks/js/state_worldmap.js"></script>
<script src="/chunks/js/state_localmap.js"></script>
<script src="/js/script.js"></script>
<script src="/chunks/js/config.js"></script>
<script src="/chunks/js/controls.js"></script>
        
<link href="/css/all.css" type="text/css">
<link rel="stylesheet" type="text/css" href="/libs/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/css/all.css" />
<link rel="stylesheet" type="text/css" href="/css/brands.css" />
<link rel="stylesheet" type="text/css" href="/css/style.css" />