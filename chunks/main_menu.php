<style>
    #screen_login {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: #000;
        z-index: 5;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    #screen_login img {
        position: absolute;
        display: block;
        -webkit-animation: rotation 120s infinite linear;
        animation: rotation 120s infinite linear;
    }

    .abs {
        position: absolute;
    }

    .abs h1,
    .abs h2 {
        text-align: center;
        font-family: "Poiret One", sans-serif;
        color: #2B2;
        text-shadow: 0 0 16px #2B2;
    }

    .abs h1 {
        font-size: 5rem;
    }

    .abs h2 {
        font-size: 3rem;
        line-height: 1rem;
    }


    #win_login,
    #win_register {
        border-radius: 32px;
        margin: 0 auto;
        text-align: center;
    }

    #ui_login input {
        font-family: "Poiret One", sans-serif;
        font-weight: bold;
        display: block;
        width: 240px;
        background: 0;
        border-radius: 8px 32px;
        padding: 4px 12px;
        border: 1px #2B2 solid;
        margin: 0 auto 8px;
        color: #2B2;
        transition: all 0.15s ease;
        box-sizing: border-box;
    }

    #ui_login hr {
        width: 240px;
        border: 0;
        border-top: 1px solid #3CF800;
        margin: 16px auto;
    }

    #ui_login input:hover {
        background-color: #2B2;
        color: #000;
    }

</style>

<div id="screen_login">
    <img src="/i/reverseGear.png">
    <div class="abs">
        <h2>Cogs & Copper:</h2>
        <h1>FALLOUTЪ</h1>
        <div id="ui_login">
            <?= $App->getChunk("login"); ?>
            <hr />
            <?= $App->getChunk("register"); ?>
        </div>
    </div>
</div>
