<div id="win_character" class="row">
    <div id="win_special" class="col-md-4">
        <p>Strength: 5</p>
        <p>Perception: 5</p>
        <p>Endurance: 5</p>
        <p>Charisma: 5</p>
        <p>Intelligence: 5</p>
        <p>Agility: 5</p>
        <p>Luck: 5</p>
    </div>
    <div id="win_skills" class="col-md-8">
        <p>Small Guns</p>
        <p>Big Guns</p>
        <p>Energy Weapons</p>
        <p>Unarmed</p>
        <p>Melee Weapons</p>
        <p>Throwing</p>
        <p>First Aid</p>
        <p>Doctor</p>
        <p>Sneak</p>
        <p>Lockpick</p>
        <p>Steal</p>
        <p>Traps</p>
        <p>Science</p>
        <p>Repair</p>
        <p>Speech</p>
        <p>Barter</p>
        <p>Gambling</p>
        <p>Outdoorsman</p>
    </div>
</div>