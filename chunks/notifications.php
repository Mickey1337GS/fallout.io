<style>
    #notifications {
        position: fixed;
        bottom: 0;
        right: 0;
        z-index: 10;
    }
    .note { padding: 4px 16px; border-radius: 16px; margin: 4px; }
    .notice { background: #ffae5c; }
    .error { background: #ff3b3b; }
    .ok { background: #73ff6e; }
</style>

<div id="notifications"></div>
