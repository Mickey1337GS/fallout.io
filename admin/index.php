<?php

set_include_path( $_SERVER["DOCUMENT_ROOT"] );
require_once("app/core.php");

?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8" />
        
        <link rel="shortcut icon" href="/i/favicon.ico" type="image/x-icon" />
        <link rel="icon" href="/i/favicon.ico" type="image/x-icon" />

        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <script src="/libs/jquery-3.3.1.min.js"></script>
        <script src="/libs/phaser.min.js"></script>
        <script src="/libs/socket.io.slim.js"></script>

        <script src="/js/core.js"></script>
        <script src="/chunks/js/config.js"></script>

        <link rel="stylesheet" href="/css/fonts.css" type="text/css" />
        <link rel="stylesheet" href="/css/all.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="/css/all.css" />
        <link rel="stylesheet" type="text/css" href="/css/brands.css" />
        <link rel="stylesheet" type="text/css" href="/css/style.css" />
        <style>
            * { padding: 0; margin: 0; }
            body { min-height: 100vh; }
            .container { max-width: 1140px; margin: 0 auto; }
            .row { display: flex; width: 100%; color: #FFF; }
            .left { flex: 2; }
            .right { flex: 1; }
            form input { display: block; padding: 4px; box-sizing: border-box; width: 100%; }
            span { word-break: break-all; }
        </style>
    </head>
    
    <body>
        <div class="container">
            <header class="row">
            </header>
            <div class="row">
                <div class="left">
                    <div id="console">
                        <?php

                        $App->connectToDB();

                        if( !$App->useDB( DB_MAIN ) ) {
                            $App->createDB( DB_MAIN );
                            if( !$App->useDB( DB_MAIN ) ) { die(); }
                        }

                        if ( !$App->ifTableExists("players") ) {
                            $App->makeTable( "players" );
                            $App->addAdmin();
                            
                            $App->addPlayer(
                                "The Chosen One",
                                "downtherabbithole",
                                "realchosenone69@matrix.com"
                            );
                        }
                        
                        if( !empty( $_POST ) ) {
                            $App->addPlayer(
                                $_POST["name"],
                                $_POST["password"],
                                $_POST["email"]
                            );
                        }

                        $App->getTable( "players" );

                        ?>
                    </div>
                </div>
                <div class="right">
                    <form action="" method="post">
                        <label>CREATE NEW USER</label>
                        <input type="text" name="name" placeholder="Account name" />
                        <input type="email" name="email" placeholder="E-mail" />
                        <input type="password" name="password" placeholder="Password" />
                        <input type="submit" value="new" />
                    </form>
                </div>
            </div>
            <footer class="row">
            </footer>
        </div>
    </body>
    
</html>