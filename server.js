/* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
// Include modules
var http = require('http'); // Web-Server for data transfer (not to display pages)
var https = require('https'); // Web-Server for data transfer (not to display pages)
var fs = require('fs'); // File system
var io = require('socket.io'); // For persistent connections and real-time
var mysql = require('mysql'); // For player database management
var cs = require("./app/kernel/console_styles.js"); // Console styling (colors, background etc)

// SetUp variables
const version = "1.0.0";

// You can change the port you want your server to run on.
// Mind the NAT settings if you are using a router.
// If you change the port here you must also change it in config.php file.
const port = 1337;
const config = {
    DOMAIN: "fallout.gq"
};

const log = console.log;

const pretext_welcome = "Welcome to Cogs & Copper: Falloutъ Online! version: " + version;
const pretext_start = pretext_welcome + " For the best experience please use Google Chrome or other webkit browser.";

/* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
// Console control. USAGE (example): 701 This message will be displayed to all online players  //
/* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
const console_clear = "\x1B[2J\x1B[0f"; // This clears console if written
process.stdout.write(console_clear); // Clear console
const stdin = process.openStdin(); // Shortcut for console logging
stdin.setEncoding('utf-8'); // Set encoding
stdin.addListener("data", inputManager, this); // Assign listener function to "data" event of console
function inputManager(d) { // Deckare a listener function for console control
    let t = d.toString().trim(); // Remove whitespaces from both ends
    let code = t.slice(0, 3); // Get first three letters (which are numbers)
    let data = t.slice(3).trim(); //
    switch (code) {
        case "1":
            break;
        case "700":
            io.emit("700_confirm", {
                data: data
            }); // Confirm box (with "YES" and "NO" buttons)
            break;
        case "701":
            io.emit("701_alert", {
                data: data
            }); // Alert box (with one button "DONE")
            break;
        case "666":
            io.emit("300_kick"); // Kick all players by realoding page
            break;
        default:
            break;
    };
};

/* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
// Connect to MySQL database
var con = mysql.createConnection({
    host: "127.0.0.1",
    user: "fallout",
    password: "password"
});

con.connect(function (err) {
    if (err) throw err;
    log(cs.pattern_green(), "Connection to the MySQL has been established!");

    con.query("SHOW DATABASES LIKE 'fallout_db'", function (E, R, F) {
        if (E) throw E;
        let res_num = Object.keys(R).length;
        if (res_num == 0) {
            log(cs.pattern_yellow(), "No designated database named `fallout_db` found. Trying to create one...");
            con.query("CREATE DATABASE fallout_db", function (E, R, F) {
                if (E) throw E;
                log(cs.pattern_green(), "Database `fallout_db` created successfully. Trying to use it now.");
            });
        } else {
            log(cs.pattern_green(), "Database `fallout_db` found. Trying to use it now.");
        }

        // USE designated database for this game. I know this is all too verbose but bare with me please;
        con.query("USE fallout_db", function (E, R, F) {
            if (E) throw E;
            log(cs.pattern_green(), "Using DB now!");
        });

        // Check if a table for active users exists
        // This is done this way for more feedback from the system (instead of for example "CREATE TABLE IF NOT EXISTS players;")
        con.query("SHOW TABLES LIKE 'players'", function (E, R, F) {
            // How many tables like `players` exist? Put the number in a variable. 0 - no tables found.
            let res_num = Object.keys(R).length;
            // If no 'players' table exists
            if (res_num == 0) {
                // Say it
                log(cs.pattern_yellow(), "No `players` table was found. Trying to create one now...");
                // Try to create said table.
                con.query("CREATE TABLE IF NOT EXISTS players( name VARCHAR(255), password VARCHAR(255), email VARCHAR(255) )", function (E, R, F) {
                    if (E) throw E;
                    // If successful - say it
                    log(cs.pattern_green(), "`players` table has been created. Continuing...");
                })
            } else {
                // Table 'players' already exists. All is OK.
                log(cs.pattern_green(), "`players` table exists. Can process registration inquiries now. Continuing...");
            }
        });
    });
});

var options = {
    key: fs.readFileSync('/etc/letsencrypt/live/' + config.DOMAIN + '/privkey.pem').toString(),
    cert: fs.readFileSync('/etc/letsencrypt/live/' + config.DOMAIN + '/fullchain.pem').toString(),
    secure: true
};

var server = https.createServer(options);
server.listen(port);

/* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
// Start server
io = io(server);

log(cs.pattern_start(), 'Cogs & Copper Server v' + version + ' - Dust');
log(cs.pattern_info(), "Port: " + port);

var clients = [];
var messages = [];
var playersOnline = []; // Array to keep logs on online players.

/* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
// IO Handlers
io.on("connection", function (s) {
    var ip = s.handshake.address;
    var player = newPlayer();
    clients.push(player);

    /* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
    /* ONLINE FUNCTIONS */

    function newPlayer() {
        // Debug values: x27 y29 instead of 4 and 6
        let tempX = Math.floor(Math.random() * 3) * 100 + 25;
        let tempY = Math.floor(Math.random() * 4) * 100 + 25;
        let pxw = (tempX - 64) / 128;
        let pyw = (tempY - 64) / 128;
        let pretext_newuser = `+Connection: ${ip}`;
        log(cs.pattern_green(), pretext_newuser);
        return {
            x: tempX,
            y: tempY,
            wX: tempX,
            wY: tempY
        };
    };

    function gotOnline() {
        s.emit("start_game");
        s.broadcast.emit("newPlayer", {
            data: player
        });
        s.emit("701_alert", {
            data: pretext_start
        });
        s.emit("100_chat_message", {
            data: pretext_welcome
        });
        s.emit("100_chat_message", {
            data: "Players online: " + clients.length
        });
        s.emit("start", {
            a: clients,
            i: clients.indexOf(player),
            m: messages.slice(0, 9).reverse()
        });
    };

    function sendChat(e) {
        let t = new Date().toTimeString().split(' ')[0];
        let msg = e.data.replace(/<[^>]+>/g, '').trim().substring(0, 81);
        let name = player.name;
        log(cs.pattern_blue(), ip + " [" + t + "] " + name + ": " + msg)
        if (msg !== "") {
            let tmsg = "[" + t + "] " + name + ": " + msg;
            messages.push(tmsg);
            io.emit("100_chat_message", {
                data: tmsg
            });
        };
    };

    function setWaypoint(d) {
        let i = clients.indexOf(player);
        if (d.x > 1400) {
            d.x = 1400
        };
        if (d.y > 1500) {
            d.y = 1500
        };
        if (d.x < 0) {
            d.x = 0
        };
        if (d.y < 0) {
            d.y = 0
        };
        log(cs.pattern_yellow(), "Player " + i + " set waypoint to: " + d.x + "/" + d.y);
        player.wX = d.x;
        player.wY = d.y;
        io.emit("setWaypoint", {
            x: d.x,
            y: d.y,
            i: i
        });
    };

    function updatePlayer(d) {
        let i = clients.indexOf(player);
        player.x = d.x;
        player.y = d.y;
        s.broadcast.emit('updatePlayer', {
            x: d.x,
            y: d.y,
            wx: player.wX,
            wy: player.wY,
            i: i
        });
    };

    function disconnect() {
        let i = clients.indexOf(player);
        log(cs.pattern_red(), '-Connection: ' + ip);
        s.broadcast.emit("player_left", {
            i: i
        });
        clients.splice(i, 1);
        playersOnline.splice(playersOnline.indexOf(player.name), 1);
    };

    function enter(d) {
        s.emit("enter", {
            level: "desert"
        });
        s.broadcast.emit("playerEnter", {
            i: clients.indexOf(player)
        });
    };

    function cmd_login(d) {
        // Console notice.
        log(cs.pattern_blue(), "~TRY LOGIN: " + d.l + " from: " + ip);

        // Check if user already online.
        if (playersOnline.indexOf(d.l) != -1) {
            log(cs.pattern_red(), "-LOGIN FAILED (User already online): " + d.l + " from: " + ip);
            s.emit("login_NO", {
                errorcode: 1
            });
            return false;
        }

        let query = `SELECT name, password FROM players WHERE name = '${d.l}' LIMIT 1`;
        con.query(query, function (E, R, F) {
            if (E) throw E;

            if (R.length == 0) {
                log(cs.pattern_red(), "-LOGIN FAILED (No such user): " + d.l + " from: " + ip);
                s.emit("login_NO", {
                    errorcode: 0
                });
                return false;
            }

            if (d.p != R[0].password) {
                log(cs.pattern_red(), "-LOGIN FAILED (Incorrect password): " + d.l + " from: " + ip);
                s.emit("login_NO", {
                    errorcode: 2
                });
                return false;
            }

            log(cs.pattern_green(), "+LOGIN OK: " + d.l + " from: " + ip);
            playersOnline.push(d.l);
            player.name = d.l;
            s.emit("login_OK");
        });
    }

    function cmd_register(d) {
        // Console notice.
        log(cs.pattern_blue(), "~TRY REGISTER: " + d.l + " from: " + ip);

        if (d.l.trim() === "") {
            log(cs.pattern_red(), "-REG FAILED (Not suitable username): " + d.l + " from: " + ip);
            s.emit("reg_NO", {
                errorcode: 3
            });
            return false;
        }

        if (d.p.trim() === "") {
            log(cs.pattern_red(), "-REG FAILED (Not suitable password): " + d.p + " from: " + ip);
            s.emit("reg_NO", {
                errorcode: 2
            });
            return false;
        }

        if (d.e.trim() === "") {
            log(cs.pattern_red(), "-REG FAILED (Not suitable email): " + d.e + " from: " + ip);
            s.emit("reg_NO", {
                errorcode: 5
            });
            return false;
        }

        let query = `SELECT name FROM players WHERE name = '${d.l}' LIMIT 1`;
        con.query(query, function (E, R, F) {
            if (E) throw E;

            if (R.length != 0) {
                log(cs.pattern_red(), "-REG FAILED (User already exists): " + d.l + " from: " + ip);
                s.emit("reg_NO", {
                    errorcode: 1
                });
                return false;
            }

            let query = `INSERT INTO players ( name, password, email ) VALUES ( '${d.l}','${d.p}','${d.e}' )`;
            con.query(query, function (E, R, F) {
                if (E) throw E;
                log(cs.pattern_green(), "+NEW USER REG: " + d.l + " from: " + ip);
                s.emit("reg_OK");
            });
        });
    }

    /* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */
    /* ONLINE HANDLERS */

    s.on("101_send_chat", sendChat, this);
    s.on("setWaypoint", setWaypoint, this);
    s.on("updateMe", updatePlayer, this);
    s.on("disconnect", disconnect, this);
    s.on("enter", enter, this);
    s.on("cmd_login", cmd_login, this);
    s.on("cmd_register", cmd_register, this);
    s.on("ready_world", gotOnline, this);

    /* --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- */

});
