<?php

define( "GAME_TITLE", "Cogs & Copper: Falloutъ" );
define( "GAME_VERSION", "1.0.0 - Dust" );

// You can change the port you want your server to run on.
// Mind the NAT settings if you are using a router.
// If you change the port here you must also change it in server.js file.
define( "GAME_PORT", 1337 );

// Change this to your server's IP address.
define( "GAME_IP", "192.168.1.65" );
define( "DB_USER", "fallout" );
define( "DB_PASS", "password" );
define( "DB_MAIN", "fallout_db" );
define( "DB_ENGINE", "INNODB" );

?>